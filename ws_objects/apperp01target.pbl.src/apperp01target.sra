﻿$PBExportHeader$apperp01target.sra
$PBExportComments$Generated Application Object
forward
global type apperp01target from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type apperp01target from application
string appname = "apperp01target"
string appruntimeversion = "21.0.0.1288"
end type
global apperp01target apperp01target

on apperp01target.create
appname = "apperp01target"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on apperp01target.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

